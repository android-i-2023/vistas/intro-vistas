package com.utn.ejemplo

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible

// Toda Activity debe heredar de la clase Activity o alguna de sus subclases
// En este momento se recomienda heredar de AppCompatActivity, ya que se actualiza muy seguido,
// mientras que Activity no se actualiza casi nunca
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main) // enlaza esta Activity con activity_main.xml

        // Como en esta Activity hay dos ejemplos, para que el código sea más claro,
        // se configuran en dos funciones separadas
        configurarEjemploButtonClick()
        configurarEjemploEditText()
    }

    private fun configurarEjemploButtonClick() {
        // Obtenemos referencias a las vistas que necesitamos del XML.
        // Como los ids son recursos de la app, los accedemos por medio de R.id (que es una  clase)
        val texto: TextView = findViewById(R.id.texto1)
        val boton: Button = findViewById(R.id.boton1)

        // Este objeto va a determinar qué hace la aplicación cuando el botón es clickeado.
        // - Como View.OnClickListener es una interfaz, necesitamos definir su único método, onClick().
        // - Como View.OnClickListener no es una clase, no hay paréntesis en la línea de abajo.
        // - Como sólo queremos un único objeto de esta nueva clase, usamos la palabra clave object.
        // - Esta clase no tiene nombre.
        val clickListener = object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (texto.isVisible) {
                    // View.INVISIBLE oculta la vista manteniendo su tamaño y posición
                    texto.visibility = View.INVISIBLE

                } else {
                    texto.visibility = View.VISIBLE
                }
            }
        }
        boton.setOnClickListener(clickListener)
    }

    private fun configurarEjemploEditText() {
        // Obtenemos referencias a las vistas que necesitamos del XML.
        // Como los ids son recursos de la app, los accedemos por medio de R.id (que es una  clase)
        val texto: TextView = findViewById(R.id.texto2)
        val campoDeTexto: EditText = findViewById(R.id.campo_de_texto)

        // Este objeto va a determinar qué hace la aplicación cuando el texto del EditText cambia.
        // Como TextWatcher es una interfaz y estamos creando un objeto con ella, necesitamos definir sus tres métodos.
        // Usamos el método onTextChanged, también podríamos haber usado afterTextChanged
        // Este objeto pertenece a una clase sin nombre, con una única instancia, y que implementa la clase TextWatcher
        // (ver comentario en configurarEjemploButtonClick()
        val textWatcher = object: SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                texto.text = campoDeTexto.text
            }
        }
        campoDeTexto.addTextChangedListener(textWatcher)
    }
}

open class SimpleTextWatcher: TextWatcher {
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
    override fun afterTextChanged(s: Editable?) = Unit
}
